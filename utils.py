import re
from statistics import mean

TAG_RE = re.compile(r'<[^>]+>')
Special_pattern = ["[CONVERTBACKTOHTML]", "[/CONVERTBACKTOHTML]", "[start_vnpt_text_class]", "[end_vnpt_text_class]"]
special_message =["\\N", "[like]", "[dislike]", "[FILE]"]
def remove_tags(text):
    #text = TAG_RE.sub('', text)
    for pattern in Special_pattern:
        text = text.replace(pattern, "")
    return TAG_RE.sub('', text)


def to_dialogs(df):
    dialogs = {}
    for row in df.iterrows():
        if row[1]["CHAT_ID"] not in dialogs.keys():
            dialogs[row[1]["CHAT_ID"]] = [{"author_id": row[1]["AUTHOR_ID"], "message": row[1]["MESSAGE"], "time": row[1]["DATE_CREATE"]}]
        else:
            dialogs[row[1]["CHAT_ID"]].append({"author_id": row[1]["AUTHOR_ID"], "message": row[1]["MESSAGE"], "time": row[1]["DATE_CREATE"]})
    return dialogs


def split_dialog_to_section(dialogs):
    new_dialogs = {}
    for chat_id, dialog in dialogs.items():
        total_dialog = []
        one_dialog = []
        for i in range(len(dialog)):
            if dialog[i]["message"].startswith("Cuộc hỗ trợ #[URL") and dialog[i]["message"].endswith("đã bắt đầu") and i > 2:
                total_dialog.append(one_dialog)
                one_dialog = [dialog[i]]
            elif i == len(dialog) - 1:
                one_dialog.append(dialog[i])
                total_dialog.append(one_dialog)
            else:
                one_dialog.append(dialog[i])
        new_dialogs[chat_id] = total_dialog
    return new_dialogs


def get_staff_id(message):
    k = 6
    while message[k] != "]":
        k += 1
    staff_id = message[6:k]
    return int(staff_id)


def get_staff_ids(dialogs):
    staff_ids = []
    for id, dialog in dialogs.items():
        for section in dialog:
            for i in range(len(section)-1, -1, -1):
                if section[i]["message"].startswith("[USER=") and section[i]["message"].endswith("đã chọn cuộc trò chuyện"):
                    staff_id = get_staff_id(section[i]["message"])
                    if staff_id not in staff_ids:
                        staff_ids.append(staff_id)
    return staff_ids

def counts_dialogs_number(dialogs):
    # count number dialogs staff and customer
    total_chat = 0
    no_reply = 0
    no_chat = 0
    num_message = 0
    len_message = []
    messages = []
    for id, dialog in dialogs.items():
        for section in dialog:
            check = 0
            for i in range(len(section)-1, -1, -1):
                if section[i]["message"].startswith("[USER=") and section[i]["message"].endswith("đã chọn cuộc trò chuyện"):
                    total_chat += 1
                    check += 1
                    authorSet = []
                    for k in range(i+1, len(section)):
                        if section[k]["author_id"] not in authorSet:
                            authorSet.append(section[k]["author_id"])
                    #print(authorSet)
                    if len(authorSet) == 2:
                        no_reply += 1
                    elif len(authorSet) > 2:
                        for k in range(i+1, len(section)):
                            if section[k]["author_id"] != 0:
                                num_message += 1
                                if section[k]["message"] not in special_message:
                                    len_message.append(len(section[k]["message"].split()))
                                    messages.append(section[k]["message"])
                    break
            if check == 0:
                no_chat += 1
    return total_chat, no_reply, no_chat, len_message


def print_section(section, staff_ids):
    for message in section :
        text = remove_tags(message["message"])
        if message["author_id"] == 0:
            print(f"Bot:    {text}")
        elif message["author_id"] in staff_ids:
            print(f"Staff:  {text}")
        else:
            print(f"Client: {text}")


def print_dialog(dialogs, dialogs_id, staff_ids):
    for section in dialogs[dialogs_id]:
        print_section(section, staff_ids)
        print("------------------------------------------------------------")


def count_like_dislike(dialogs):
    count_like = 0
    count_dislike = 0
    for id, dialog in dialogs.items():
        for section in dialog:
            for message in section:
                if message["message"] == "[like]":
                    count_like += 1
                elif message["message"] == "[dislike]":
                    count_dislike += 1
                    
    return count_like, count_dislike


def count_len_message_user(dialogs, staff_ids):
    
    user_messages = []
    for id, dialog in dialogs.items():
        for section in dialog:
            for i in range(len(section)-1, -1, -1):
                if section[i]["message"].startswith("[USER=") and section[i]["message"].endswith("đã chọn cuộc trò chuyện"):
                    for k in range(i+1, len(section)):
                        if section[k]["author_id"] not in staff_ids and section[k]["author_id"] != 0:
                            if section[k]["message"] not in special_message:
                                user_messages.append(section[k]["message"])
    len_user_message = [len(message.split()) for message in user_messages]
    return len_user_message


def return_statics(data):
    print(f"mean: {mean(data)}")
    print(f"max: {max(data)}")
    print(f"min: {min(data)}")


def return_statics_turn(dialogs):
    turn_bots_dialogs = []
    turn_staffs_dialogs = []
    for id, dialog in dialogs.items():
        turn_bots = []
        turn_staffs = []
        for section in dialog:
            turn_bot = 0
            turn_staff = 0
            end = 0
            for i in range(len(section)-1, -1, -1):
                if section[i]["message"].startswith("[USER=") and section[i]["message"].endswith("đã chọn cuộc trò chuyện"):
                    end = i
            for i in range(0, end-1):
                if section[i]["author_id"] != section[i+1]["author_id"]:
                    turn_bot += 1
                    i += 1
            for i in range(end+1, len(section)-1):
                if section[i]["author_id"] != section[i+1]["author_id"]:
                    turn_staff += 1
                    i += 1
            turn_bots.append(turn_bot)
            turn_staffs.append(turn_staff)
        turn_bots_dialogs.append(turn_bots)
        turn_staffs_dialogs.append(turn_staffs)
    flat_turn_bots = [turn for section in turn_bots_dialogs for turn in section]
    flat_turn_staffs = [turn for section in turn_staffs_dialogs for turn in section]
    return flat_turn_bots, flat_turn_staffs